<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Entity;

interface ProductInterface
{
    public function getId(): ?int;

    public function isEnabled(): bool;

    public function getTitle(): ?string;

    public function getContent();

    public function getCreated(): ?\DateTime;

    public function getCreator();
}
