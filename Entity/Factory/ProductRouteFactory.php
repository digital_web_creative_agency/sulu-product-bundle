<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Entity\Factory;

use Sulu\Bundle\RouteBundle\Manager\RouteManager;
use Sulu\Bundle\RouteBundle\Model\RouteInterface;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;

class ProductRouteFactory implements ProductRouteFactoryInterface
{
    private RouteManager $routeManager;

    /**
     * ProductFactory constructor.
     */
    public function __construct(
        RouteManager $manager
    ) {
        $this->routeManager = $manager;
    }

    public function generateProductRoute(Product $product): RouteInterface
    {
        return $this->routeManager->create($product);
    }

    public function updateProductRoute(Product $product, string $routePath): RouteInterface
    {
        return $this->routeManager->update($product, $routePath);
    }
}
