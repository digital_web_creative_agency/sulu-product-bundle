<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Entity\Factory;

use Sulu\Bundle\TagBundle\Tag\TagManagerInterface;
use Sulu\Component\Persistence\RelationTrait;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;

class TagFactory extends AbstractFactory implements TagFactoryInterface
{
    use RelationTrait;
    private TagManagerInterface $tagManager;

    /**
     * TagFactory constructor.
     */
    public function __construct(TagManagerInterface $tagManager)
    {
        $this->tagManager = $tagManager;
    }

    /**
     * @return bool
     */
    public function processTags(Product $product, $tags)
    {
        $get = function ($tag) {
            return $tag->getId();
        };

        $delete = function ($tag) use ($product) {
            return $product->removeTag($tag);
        };

        $update = function () {
            return true;
        };

        $add = function ($tag) use ($product) {
            return $this->addTag($product, $tag);
        };

        $entities = $product->getTags();

        return $this->processSubEntities(
            $entities,
            $tags,
            $get,
            $add,
            $update,
            $delete
        );
    }

    /**
     * Returns the tag manager.
     *
     * @return TagManagerInterface
     */
    public function getTagManager()
    {
        return $this->tagManager;
    }

    /**
     * Adds a new tag to the given contact and persist it with the given object manager.
     *
     * @return bool True if there was no error, otherwise false
     */
    protected function addTag(Product $product, $data)
    {
        $success = true;
        $resolvedTag = $this->getTagManager()->findOrCreateByName($data);
        $product->addTag($resolvedTag);

        return $success;
    }
}
