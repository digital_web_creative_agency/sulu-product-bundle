<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Entity\Factory;

use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;

interface TagFactoryInterface
{
    public function processTags(Product $product, $tags);
}
