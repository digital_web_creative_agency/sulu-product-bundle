<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Twig;

use Doctrine\Common\Cache\Cache;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;
use DigitalWeb\Bundle\SuluProductBundle\Repository\ProductRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Extension to handle product in frontend.
 */
class ProductTwigExtension extends AbstractExtension
{
    public function __construct(
        private readonly Cache $cache,
        private readonly ProductRepository $productRepository
    ) {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('sulu_resolve_product', [$this, 'resolveProductFunction']),
        ];
    }

    public function resolveProductFunction(int $id): ?Product
    {
        if ($this->cache->contains($id)) {
            return $this->cache->fetch($id);
        }

        $product = $this->productRepository->find($id);
        if (null === $product) {
            return null;
        }

        $this->cache->save($id, $product);

        return $product;
    }
}
