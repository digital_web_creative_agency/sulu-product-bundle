<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Event;

use Sulu\Bundle\ActivityBundle\Domain\Event\DomainEvent;
use DigitalWeb\Bundle\SuluProductBundle\Admin\ProductAdmin;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;

class ProductRemovedActivityEvent extends DomainEvent
{
    private Product $product;

    private array $payload;

    public function __construct(
        Product $book,
        array $payload
    ) {
        parent::__construct();

        $this->product = $book;
        $this->payload = $payload;
    }

    public function getEventType(): string
    {
        return 'removed';
    }

    public function getResourceKey(): string
    {
        return Product::RESOURCE_KEY;
    }

    public function getResourceId(): string
    {
        return (string) $this->product->getId();
    }

    public function getEventPayload(): ?array
    {
        return $this->payload;
    }

    public function getResourceTitle(): ?string
    {
        return $this->product->getTitle();
    }

    public function getResourceSecurityContext(): ?string
    {
        return ProductAdmin::SECURITY_CONTEXT;
    }
}
