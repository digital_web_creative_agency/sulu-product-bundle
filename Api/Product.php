<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Api;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\VirtualProperty;
use Sulu\Component\Rest\ApiWrapper;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product as ProductEntity;

/**
 * The Product class which will be exported to the API.
 *
 * @ExclusionPolicy("all")
 */
class Product extends ApiWrapper
{
    public function __construct(ProductEntity $contact, $locale)
    {
        // @var ProductEntity entity
        $this->entity = $contact;
        $this->locale = $locale;
    }

    /**
     * Get id.
     *
     * @VirtualProperty
     *
     * @SerializedName("id")
     * @Groups({"fullProduct"})
     */
    public function getId(): ?int
    {
        return $this->entity->getId();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("title")
     * @Groups({"fullProduct"})
     */
    public function getTitle()
    {
        return $this->entity->getTitle();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("teaser")
     * @Groups({"fullProduct"})
     */
    public function getTeaser()
    {
        return $this->entity->getTeaser();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("content")
     * @Groups({"fullProduct"})
     */
    public function getContent(): array
    {
        if (!$this->entity->getContent()) {
            return [];
        }

        return $this->entity->getContent();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("enabled")
     * @Groups({"fullProduct"})
     */
    public function isEnabled(): bool
    {
        return true;
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("publishedAt")
     * @Groups({"fullProduct"})
     */
    public function getPublishedAt()
    {
        return $this->entity->getPublishedAt();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("route")
     * @Groups({"fullProduct"})
     */
    public function getRoute()
    {
        if ($this->entity->getRoute()) {
            return $this->entity->getRoute()->getPath();
        }

        return '';
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("tags")
     * @Groups({"fullProduct"})
     */
    public function getTags(): array
    {
        return $this->entity->getTagNameArray();
    }

    /**
     * Get the contacts avatar and return the array of different formats.
     *
     * @VirtualProperty
     *
     * @SerializedName("header")
     * @Groups({"fullProduct"})
     */
    public function getHeader(): array
    {
        if ($this->entity->getHeader()) {
            return [
                'id' => $this->entity->getHeader()->getId(),
            ];
        }

        return [];
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("authored")
     * @Groups({"fullProduct"})
     */
    public function getAuthored(): \DateTime
    {
        return $this->entity->getCreated();
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("created")
     * @Groups({"fullProduct"})
     */
    public function getCreated(): \DateTime
    {
        return $this->entity->getCreated();
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("changed")
     * @Groups({"fullProduct"})
     */
    public function getChanged(): \DateTime
    {
        return $this->entity->getChanged();
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("author")
     * @Groups({"fullProduct"})
     */
    public function getAuthor(): int
    {
        return $this->entity->getCreator()->getId();
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("ext")
     * @Groups({"fullProduct"})
     */
    public function getSeo()
    {
        $seo = ['seo'];
        $seo['seo'] = $this->getEntity()->getSeo();

        return $seo;
    }
}
