<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Sulu\Component\SmartContent\Orm\DataProviderRepositoryInterface;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;

/**
 * Class ProductRepository.
 */
class ProductRepository extends EntityRepository implements DataProviderRepositoryInterface
{
    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Product $product): void
    {
        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush();
    }

    public function getPublishedProduct(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('n')
            ->from('ProductBundle:Product', 'n')
            ->where('n.enabled = 1')
            ->andWhere('n.publishedAt <= :created')
            ->setParameter('created', date('Y-m-d H:i:s'))
            ->orderBy('n.publishedAt', 'DESC')
        ;

        $product = $qb->getQuery()->getResult();

        if (!$product) {
            return [];
        }

        return $product;
    }

    public function findById(int $id): ?Product
    {
        $product = $this->find($id);
        if (!$product) {
            return null;
        }

        return $product;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(int $id): void
    {
        $this->getEntityManager()->remove(
            $this->getEntityManager()->getReference(
                $this->getClassName(),
                $id
            )
        );
        $this->getEntityManager()->flush();
    }

    public function findByFilters($filters, $page, $pageSize, $limit, $locale, $options = [])
    {
        return $this->getPublishedProduct();
    }
}
