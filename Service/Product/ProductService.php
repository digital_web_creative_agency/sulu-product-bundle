<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Service\Product;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Sulu\Bundle\ActivityBundle\Application\Collector\DomainEventCollectorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Factory\ProductFactory;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Factory\ProductRouteFactory;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;
use DigitalWeb\Bundle\SuluProductBundle\Event\ProductCreatedActivityEvent;
use DigitalWeb\Bundle\SuluProductBundle\Event\ProductModifiedActivityEvent;
use DigitalWeb\Bundle\SuluProductBundle\Event\ProductRemovedActivityEvent;
use DigitalWeb\Bundle\SuluProductBundle\Repository\ProductRepository;
// use Sulu\Component\DocumentManager\DocumentManagerInterface;

class ProductService implements ProductServiceInterface
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var object|string
     */
    private $loginUser;

    /**
     * @var ProductRouteFactory
     */
    private $routeFactory;

    /**
     * @var DomainEventCollectorInterface
     */
    private $domainEventCollector;

    /**
     * @var DocumentManagerInterface
     */
    private $documentManager;

    /**
     * ArticleService constructor.
     */
    public function __construct(
        ProductRepository $productRepository,
        ProductFactory $productFactory,
        ProductRouteFactory $routeFactory,
        // DocumentManagerInterface $documentManager,
        TokenStorageInterface $tokenStorage,
        DomainEventCollectorInterface $domainEventCollector
    ) {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->routeFactory = $routeFactory;
        // $this->documentManager = $documentManager;
        $this->domainEventCollector = $domainEventCollector;

        if ($tokenStorage->getToken()) {
            $this->loginUser = $tokenStorage->getToken()->getUser();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveNewProduct(array $data, string $locale): Product
    {
        try {
            $product = $this->productFactory->generateProductFromRequest(new Product(), $data, $locale);
        } catch (\Exception $e) {
        }

        /** @var Product $product */
        if (!$product->getCreator()) {
            $product->setCreator($this->loginUser->getContact());
        }
        $product->setchanger($this->loginUser->getContact());

        $this->productRepository->save($product);

        $this->routeFactory->generateProductRoute($product);

        $this->domainEventCollector->collect(new ProductCreatedActivityEvent($product, ['name' => $product->getTitle()]));
        $this->productRepository->save($product);
        $this->documentManager->publish($product, $locale);

        return $product;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateProduct($data, Product $product, string $locale): Product
    {
        try {
            $product = $this->productFactory->generateProductFromRequest($product, [], $locale, false);
        } catch (\Exception $e) {
        }

        $product->setchanger($this->loginUser->getContact());

        if ($product->getRoute()->getPath() !== $data['route']) {
            $route = $this->routeFactory->updateProductRoute($product, $data['route']);
            $product->setRoute($route);
        }

        $this->domainEventCollector->collect(new ProductModifiedActivityEvent($product, ['name' => $product->getTitle()]));
        $this->productRepository->save($product);

        return $product;
    }

    public function updateProductPublish(Product $product, array $data): Product
    {
        switch ($data['action']) {
            case 'enable':
                $product = $this->productFactory->generateProductFromRequest($product, [], null, true);

                break;

            case 'disable':
                $product = $this->productFactory->generateProductFromRequest($product, [], null, false);

                break;
        }
        $this->domainEventCollector->collect(new ProductModifiedActivityEvent($product, ['name' => $product->getTitle()]));
        $this->productRepository->save($product);

        return $product;
    }

    public function removeProduct(int $id): void
    {
        $product = $this->productRepository->findById($id);
        if (!$product) {
            throw new \Exception($id);
        }

        $this->domainEventCollector->collect(new ProductRemovedActivityEvent($product, ['name' => $product->getTitle()]));
        $this->productRepository->remove($id);
    }
}
