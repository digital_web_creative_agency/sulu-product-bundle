## 1.0.0.7 (2022-12-24)

*  fix( changelog) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/e4dc12ed288088ccee90659453cee15cbc9f13c6)
*  fix(toolbar): Checkt op permissions [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/32ab0ceda4c6083480c4ee355c6570cc690aea0f)
*  fix(controller): Resets the controller [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/2965c51057d260745ee9a0abe4992884ad6df723)
*  fix(search): Past icon aan [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/e35a96421f009c4efdac6f0e3cb513b5c4fad279)
*  merge [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/12c3f166de4bc0aa422933806e5082ce35147123)


## 1.0.0.6 (2022-12-24)

*  fix(changelog) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/b8629deb299728fa0abfd0d9a044765d70f1a046)


## toolbar (2022-12-24)

*  fix(changelog) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/4fb5d2c3d4a66ec2e1b9c181053c1de3b4c53bd9)
*  gitflow-hotfix-stash: toolbar [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/ed5affa1ebaf790a0dac35bfbaa558046a1cd431)


## 1.0.0.5 (2022-12-24)

*  fix(changelog) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/2fff0dfb12eac73d3587aae5f65fcaae2c987494)
*  fix(twig template) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/2cf8d044f1881394fca7f1046e2fefb6a8dac727)


## 1.0.0.4 (2022-12-24)

*  fix(changelog) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/aecf0927be54c5aa6a2211e1902d17c39352acdc)
*  fix(renaming files): Past bestanden aan [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/05a2d2914e2f340b8d1a150b20db476d7d1daeb9)
*  fix(readme) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/0705592824b931e03574d7dca24fb974593bdbb2)
*  fix(merge) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/fb041e86fdc0f218cc9ade2a81ea2ae99980b118)


## 1.0.0.3 (2022-12-24)

*  fix(changelog) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/0bc64a70adc5e3995d864cf644574238b68716d8)
*  fix(renaming files): Past namen aan [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/7095f375113f50278dff9794e6bc6efcd746315d)
*  fix(docs) [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/6857ed904662c8d7f643357fd086af3508be4bd8)


## 1.0.0.2 (2022-12-24)

*  fix(changelog): Adds 1.0.0.1 release to changelog [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/d8634519c9495baf6e07125bec46bfcc21b82aea)
*  fix(news bundle): test [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/0f8479f9a7e0c360d805cb9d45744d3a6efaa4ea)


## 1.0.0.1 (2022-12-21)

*  fix(files): Renames files [View](https://bitbucket.org/digital_web_creative_agency/sulu-product-bundle/commits/305ed6be15da4ea90024076962adbe49f552e009)


