<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\DependencyInjection;

use Sulu\Bundle\PersistenceBundle\DependencyInjection\PersistenceExtensionTrait;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use DigitalWeb\Bundle\SuluProductBundle\Admin\ProductAdmin;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ProductExtension extends Extension implements PrependExtensionInterface
{
    use PersistenceExtensionTrait;

    /**
     * Allow an extension to prepend the extension configurations.
     */
    public function prepend(ContainerBuilder $container): void
    {
        if ($container->hasExtension('sulu_search')) {
            $container->prependExtensionConfig(
                'sulu_search',
                [
                    'indexes' => [
                        'product' => [
                            'name' => 'Product',
                            'icon' => 'su-folder',
                            'view' => [
                                'name' => ProductAdmin::NEWS_EDIT_FORM_VIEW,
                                'result_to_view' => [
                                    'id' => 'id',
                                    'locale' => 'locale',
                                ],
                            ],
                            'security_context' => ProductAdmin::SECURITY_CONTEXT,
                        ],
                    ],
                ]
            );
        }

        if ($container->hasExtension('sulu_route')) {
            $container->prependExtensionConfig(
                'sulu_route',
                [
                    'mappings' => [
                        Product::class => [
                            'generator' => 'schema',
                            'options' => ['route_schema' => '/product/{object.getId()}'],
                            'resource_key' => Product::RESOURCE_KEY,
                        ],
                    ],
                ]
            );
        }

        if ($container->hasExtension('sulu_admin')) {
            $container->prependExtensionConfig(
                'sulu_admin',
                [
                    'lists' => [
                        'directories' => [
                            __DIR__.'/../Resources/config/lists',
                        ],
                    ],
                    'forms' => [
                        'directories' => [
                            __DIR__.'/../Resources/config/forms',
                        ],
                    ],
                    'resources' => [
                        'product' => [
                            'routes' => [
                                'list' => 'app.get_product',
                                'detail' => 'app.get_product',
                            ],
                        ],
                    ],
                    'field_type_options' => [
                        'selection' => [
                            'product_selection' => [
                                'default_type' => 'list_overlay',
                                'resource_key' => Product::RESOURCE_KEY,
                                'view' => [
                                    'name' => 'app.product_edit_form',
                                    'result_to_view' => [
                                        'id' => 'id',
                                    ],
                                ],
                                'types' => [
                                    'auto_complete' => [
                                        'display_property' => 'title',
                                        'search_properties' => ['title'],
                                    ],
                                    'list_overlay' => [
                                        'adapter' => 'table',
                                        'list_key' => 'product',
                                        'display_properties' => ['title'],
                                        'label' => 'sulu_product.product_select',
                                        'icon' => 'su-productpaper',
                                        'overlay_title' => 'sulu_product.single_product_selection_overlay_title',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]
            );
        }

        $container->prependExtensionConfig(
            'sulu_product',
            ['templates' => ['view' => 'product/index.html.twig']]
        );

        $container->loadFromExtension('framework', [
            'default_locale' => 'en',
            'translator' => ['paths' => [__DIR__.'/../Resources/config/translations/']],
            // ...
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');
        $loader->load('controller.xml');

        $this->configurePersistence($config['objects'], $container);
    }
}
