<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;
use DigitalWeb\Bundle\SuluProductBundle\Repository\ProductRepository;

/**
 * This is the class that validates and merges configuration from your app/config files.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('sulu_product');
        $root = $treeBuilder->getRootNode();

        $root->children()
            ->arrayNode('objects')
            ->addDefaultsIfNotSet()
            ->children()
            ->arrayNode('product')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('model')->defaultValue(Product::class)->end()
            ->scalarNode('repository')->defaultValue(ProductRepository::class)->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
