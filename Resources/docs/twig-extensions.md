### Twig-Extensions

#### sulu_resolve_product

Returns product for given id.

 ```php
{% set product = sulu_resolve_product('1') %}
{{ product.title }}
 ```

Arguments:

    id: int - The id of requested product.

Returns:

    object - Object with all needed properties, like title
