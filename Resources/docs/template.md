### Twig-Extensions

If the bundle default controller is used. A template must be created in `product/index.html.twig`.

#### Example template
 ```php
{% block content %}
<h2>{{ product.title }}</h2>

    {% set header = sulu_resolve_media(product.header.id, 'de') %}
    <img src="{{ header.thumbnails['sulu-260x'] }}" alt="{{ header.title }}" title="{{ header.title }}" />

    <p>{{ product.teaser }}</p>
    
    {% for contentItem in product.content  %}
        {% if contentItem.type == 'editor'  %}
            <p>{{ contentItem.text | raw }}</p>
        {% endif %}
    {% endfor %}
{% endblock %}
 ```
