## Installation

### Install the bundle 

Execute the following [composer](https://getcomposer.org/) command to add the bundle to the dependencies of your 
project:

```bash

composer require digital-web/sulu-product-bundle

```

### Enable the bundle 
 
 Enable the bundle by adding it to the list of registered bundles in the `config/bundles.php` file of your project:
 
 ```php
 return [
     /* ... */
     DigitalWeb\Bundle\SuluProductBundle\ProductBundle::class => ['all' => true],
 ];
 ```

### Update schema
```shell script
bin/console doctrine:schema:update --force
```

## Bundle Config
    
Define the Admin Api Route in `routes_admin.yaml`
```yaml
sulu_product.admin:
  type: rest
  resource: sulu_product.rest.controller
  prefix: /admin/api
  name_prefix: app.
```

## Template
After the installation, a product [Template](template.md) must be set up for the frontend.
