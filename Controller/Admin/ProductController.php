<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Controller\Admin;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\Exception\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use DigitalWeb\Bundle\SuluProductBundle\Admin\DoctrineListRepresentationFactory;
use DigitalWeb\Bundle\SuluProductBundle\Api\Product as ProductApi;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;
use DigitalWeb\Bundle\SuluProductBundle\Repository\ProductRepository;
use DigitalWeb\Bundle\SuluProductBundle\Service\Product\ProductService;

class ProductController extends AbstractRestController implements ClassResourceInterface
{
    // serialization groups for contact
    protected static $oneProductSerializationGroups = [
        'partialMedia',
        'fullProduct',
    ];

    /**
     * @var ProductRepository
     */
    private $repository;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var DoctrineListRepresentationFactory
     */
    private $doctrineListRepresentationFactory;

    /**
     * ProductController constructor.
     */
    public function __construct(
        ViewHandlerInterface $viewHandler,
        TokenStorageInterface $tokenStorage,
        ProductRepository $repository,
        ProductService $productService,
        DoctrineListRepresentationFactory $doctrineListRepresentationFactory,
    ) {
        parent::__construct($viewHandler, $tokenStorage);

        $this->repository = $repository;
        $this->productService = $productService;
        $this->doctrineListRepresentationFactory = $doctrineListRepresentationFactory;
    }

    public function cgetAction(Request $request): Response
    {
        $locale = $request->query->get('locale');
        $listRepresentation = $this->doctrineListRepresentationFactory->createDoctrineListRepresentation(
            Product::RESOURCE_KEY,
            [],
            ['locale' => $locale]
        );

        return $this->handleView($this->view($listRepresentation));
    }

    public function getAction(int $id, Request $request): Response
    {
        if (!$entity = $this->repository->findById($id)) {
            throw new NotFoundHttpException();
        }

        $apiEntity = $this->generateApiProductEntity($entity, $this->getLocale($request));

        $view = $this->generateViewContent($apiEntity);

        return $this->handleView($view);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postAction(Request $request): Response
    {
        $product = $this->productService->saveNewProduct($request->request->all(), $this->getLocale($request));

        $apiEntity = $this->generateApiProductEntity($product, $this->getLocale($request));

        $view = $this->generateViewContent($apiEntity);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/product/{id}")
     */
    public function postTriggerAction(int $id, Request $request): Response
    {
        $product = $this->repository->findById($id);
        if (!$product) {
            throw new NotFoundHttpException();
        }
        
        $product = $this->productService->updateProductPublish($product, $request->query->all());
        $apiEntity = $this->generateApiProductEntity($product, $this->getLocale($request));
        $view = $this->generateViewContent($apiEntity);

        return $this->handleView($view);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function putAction(int $id, Request $request): Response
    {
        $entity = $this->repository->findById($id);
        if (!$entity) {
            throw new NotFoundHttpException();
        }

        $updatedEntity = $this->productService->updateProduct($request->request->all(), $entity, $this->getLocale($request));
        $apiEntity = $this->generateApiProductEntity($updatedEntity, $this->getLocale($request));
        $view = $this->generateViewContent($apiEntity);

        return $this->handleView($view);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction(int $id): Response
    {
        try {
            $this->productService->removeProduct($id);
        } catch (\Exception $tnfe) {
            throw new EntityNotFoundException(self::$entityName, $id);
        }

        return $this->handleView($this->view());
    }

    public static function getPriority(): int
    {
        return 0;
    }

    protected function generateApiProductEntity(Product $entity, string $locale): ProductApi
    {
        return new ProductApi($entity, $locale);
    }

    protected function generateViewContent(ProductApi $entity): View
    {
        $view = $this->view($entity);
        $context = new Context();
        $context->setGroups(static::$oneProductSerializationGroups);

        return $view->setContext($context);
    }
}
