<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Content;

use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\SimpleContentType;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;
use DigitalWeb\Bundle\SuluProductBundle\Repository\ProductRepository;

class ProductSelectionContentType extends SimpleContentType
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        parent::__construct('product_selection', []);

        $this->productRepository = $productRepository;
    }

    /**
     * @return Product[]
     */
    public function getContentData(PropertyInterface $property): array
    {
        $ids = $property->getValue();

        $product = [];
        foreach ($ids ?: [] as $id) {
            $singleProduct = $this->productRepository->findById((int) $id);
            if ($singleProduct) {
                $product[] = $singleProduct;
            }
        }

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    public function getViewData(PropertyInterface $property)
    {
        return [
            'ids' => $property->getValue(),
        ];
    }
}
