<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluProductBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluProductBundle\Content;

use JMS\Serializer\Annotation as Serializer;
use Sulu\Component\SmartContent\ItemInterface;
use DigitalWeb\Bundle\SuluProductBundle\Entity\Product;

class ProductDataItem implements ItemInterface
{
    /**
     * @var Product
     *
     * @Serializer\Exclude
     */
    private $entity;

    /**
     * ProductDataItem constructor.
     */
    public function __construct(Product $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @Serializer\VirtualProperty
     */
    public function getId()
    {
        return $this->entity->getId();
    }

    /**
     * @Serializer\VirtualProperty
     */
    public function getTitle()
    {
        return $this->entity->getTitle();
    }

    /**
     * @Serializer\VirtualProperty
     */
    public function getImage()
    {
        return $this->entity->getHeader();
    }

    /**
     * @return mixed|Product
     */
    public function getResource()
    {
        return $this->entity;
    }
}
